# Name: Assignment 4 Question 1
# Time: 22:07 11.30.2018 (KST)
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: Animal class
# Created by D.python
# This program has its readme file with it named A4Q1.txt

class Animal(object):                   # PARENT CLASS

    def __init__(self, noise, eat):
        self.noise=noise
        self.eat=eat
    #    self.sleep=sleep
    #    self.roam=roam

    def make_noise(self):
        return self.noise
    def eat_food(self):
        return self.eat
#    def get_sleep(self):
#        return self.sleep
#    def eat_roam(self):
#        return self.roam
    def set_noise(self, noisekind):
        self.noise=noisekind
    def set_food(self, food_name):
        self.eat=food_name
#    def set_sleep(self, sleeptime):
#        self.sleep = sleeptime
#    def set_roam(self, roamnow):
#        self.roam = roamnow

    def __str__(self):
        return "animal >> Makes noise like "+str(self.noise)+" and eats "+str(self.eat)

class Canine(Animal):   # CLASS FOR CANINE
    def dogname(self):  # HERE ONLY DOG'S NAME HAS BEEN ASKED
       print("Dog's name: Hachi")
class Hippo(Animal):    # CLASS FOR HIPPO
    pass

class Feline(Animal):   # CLASS FOR FELINE
    def catname(self):  # HERE ONLY CAT'S NAME HAS BEEN ASKED
        print("Cat's name: Abono")
while(1): # INFINITI LOOP

    your_animal=input()

    if your_animal == "Dog":
        dog=Canine("Wof Wof", "Bone")
        dog.dogname()
        print(dog.__str__())

    elif your_animal == "Wolf":
        wolf=Canine("Auu Auu", "Meat")
        print(wolf.__str__())

    elif your_animal=="Hippo":
        hippo=Hippo("POOF POOF", "Watermalon")
        print(hippo.__str__())

    elif your_animal == "Cat":
        cat=Feline("Meaw Meaw", "Milk")
        cat.catname()
        print(cat.__str__())

    elif your_animal == "Tiger":
        tiger=Feline("Roar Roar", "Meat")
        print(tiger.__str__())

    elif your_animal == "Lion":
        lion=Feline("Roar Roar", "Bufallo")
        print(lion.__str__())

    elif your_animal == "Exit":
        exit()

    else:
        print("Wrong input")